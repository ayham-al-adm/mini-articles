<div class="row text-start comment-card">
    <div class="col-2 user-section">
        {{ $comment->user->name }}
    </div>
    <div class="col-10 comment-section">
        {{ $comment->comment }}
    </div>
</div>
