@if ($errors->any())
    <div class="position-fixed top-0 end-0 p-3" style="z-index: 5">
        <div id="error-toast" class="toast align-items-center text-white bg-danger border-0 show" role="alert" aria-live="assertive" aria-atomic="true">
            <div class="d-flex">
                <div class="toast-body">
                    <ul class="mb-0">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                <button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>
            </div>
        </div>
    </div>

    <script>
        var toast = new bootstrap.Toast(document.getElementById('error-toast'));
        toast.show();
    </script>
@endif

@if (session('success'))
    <div class="position-fixed top-0 end-0 p-3" style="z-index: 5">
        <div id="message-toast" class="toast align-items-center text-white bg-success border-0 show" role="alert" aria-live="assertive" aria-atomic="true">
            <div class="d-flex">
                <div class="toast-body">
                    {{ session('success') }}
                </div>
                <button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>
            </div>
        </div>
    </div>

    <script>
        var toast = new bootstrap.Toast(document.getElementById('message-toast'));
        toast.show();
    </script>
@endif
