<!-- Sidebar -->
<div class="col-md-3 col-lg-2 sidebar">
    <div class="sidebar-sticky">
        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link active" href="/home">
                    Dashboard
                </a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown">
                    Articles
                </a>
                <ul class="dropdown-menu">
                    <li><a class="dropdown-item" href="{{ route('articles.index') }}">All</a></li>
                    <li><a class="dropdown-item" href="{{ route('articles.create') }}">Create</a></li>
                </ul>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown">
                    Comments
                </a>
                <ul class="dropdown-menu">
                    <li><a class="dropdown-item" href="{{ route('comments.index') }}">All</a></li>
                    <li><a class="dropdown-item" href="{{ route('comments.create') }}">Create</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
