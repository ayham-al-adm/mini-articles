<form action="{{ $submitUrl }}" method="POST">
    @if ($submitMethod)
        @method($submitMethod)
    @endif

    @csrf

    @foreach ($formInputs as $name => $input)
        {{-- TODO: Move types into Enums --}}
        @switch($input['type'])
            @case('textarea')
                <div class="mb-3">
                    <label class="form-label" for="{{ $name }}">@lang("inputs.{$name}")</label>
                    <textarea class="form-control" id="{{ $name }}" name="{{ $name }}">{{ isset($item) ? $item->{$name} : null }}</textarea>
                </div>
            @break

            @case('password')
                <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label">Password</label>
                    <input type="password" class="form-control" id="exampleInputPassword1">
                </div>
            @break

            @case('email')
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Email address</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                    <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
                </div>
            @break

            @case('checkbox')
                <div class="mb-3 form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Check me out</label>
                </div>
            @break

            @case('foreign')
                <div class="mb-3">
                    <label class="form-label" for="{{ $name }}">@lang("inputs.{$name}")</label>
                    {{-- TODO: Use select2 for this type of inputs --}}
                    <select class="form-control" id="{{ $name }}" name="{{ $name }}">
                        @foreach ($input['related_model']::get() as $inputOption)
                            <option value="{{ $inputOption->id }}" @if (isset($item)) {{ $item->{$name} == $inputOption->id ? 'selected' : null }} @endif>{{ $inputOption->title }}</option>
                        @endforeach
                    </select>
                </div>
            @break

            @default
                <div class="mb-3">
                    <label class="form-label" for="{{ $name }}">@lang("inputs.{$name}")</label>
                    <input value="{{ isset($item) ? $item->{$name} : null }}" type="text" class="form-control" id="{{ $name }}" name="{{ $name }}"></input>
                </div>
        @endswitch
    @endforeach

    <button type="submit" class="btn btn-primary float-end">Submit</button>
</form>
