@extends('layouts.app')

@section('content')
    <h1>Control Panel</h1>
    <div class="main-content">
        @include('_partials.form', [
            'submitUrl' => route('articles.store'),
            'submitMethod' => 'POST',
        ])
    </div>
@endsection
