@extends('layouts.app')

@section('content')
    <h1>Edit Article</h1>
    <div class="main-content">
        @include('_partials.form', [
            'submitUrl' => route('articles.update', ['article' => $article->id]),
            'submitMethod' => 'PUT',
            'item' => $article,
        ])
    </div>
@endsection
