@extends('layouts.guest')

@section('content')
    <div class="main-content text-center">
        <h1>{{ $article->title }}</h1>
        <p class="text-start"><span>Created at: </span><span>{{ $article->created_at->diffForHumans() }}</span><span> ,By: </span><span>{{ $article->user->name }}</span></p>
        <p>{!! $article->description !!}</p>

        <div class="comments-section mt-5">
            <h2 class="text-decoration-underline">Comments</h2>
            @auth
                <div class="row">
                    <form class="text-start" action="{{ route('comments.store') }}" method="POST">
                        @csrf
                        <input type="hidden" name="article_id" value="{{ $article->id }}" />
                        <div class="mb-3">
                            <textarea class="form-control" id="comment" name="comment" required placeholder="Enter your comment here"></textarea>
                        </div>
                        <div class="mb-3">
                            <button type="submit" class="btn btn-primary">Comment</button>
                        </div>
                    </form>
                </div>
            @endauth
            @foreach ($article->comments as $comment)
                @include('_partials.comment-card', ['comment' => $comment])
            @endforeach
        </div>
    </div>
@endsection
