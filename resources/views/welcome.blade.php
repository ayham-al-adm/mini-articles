@extends('layouts.guest')

@section('content')
    <div class="row">
        @foreach (\App\Models\Article::get() as $article)
            <div class="col-4 mb-4">
                <a href="{{ route('articles.show', ['article' => $article->id]) }}">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">{{ $article->title }}</h5>
                        </div>
                    </div>
                </a>
            </div>
        @endforeach
    </div>
@endsection
