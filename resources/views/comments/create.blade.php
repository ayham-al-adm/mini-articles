@extends('layouts.app')

@section('content')
    <h1>Control Panel</h1>
    <div class="main-content">
        @include('_partials.form', [
            'submitUrl' => route('comments.store'),
            'submitMethod' => 'POST',
        ])
    </div>
@endsection
