@extends('layouts.app')

@section('content')
    <h1>Edit Comment</h1>
    <div class="main-content">
        @include('_partials.form', [
            'submitUrl' => route('comments.update', ['comment' => $comment->id]),
            'submitMethod' => 'PUT',
            'item' => $comment,
        ])
    </div>
@endsection
