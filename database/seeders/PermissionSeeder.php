<?php

declare(strict_types=1);

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Permission::create([
            'name'          => 'articles.approve.*',
            'guard_name'    => 'web',
            'created_at'    => now(),
            'updated_at'    => now(),
        ]);

        Permission::create([
            'name'          => 'articles.approve.own',
            'guard_name'    => 'web',
            'created_at'    => now(),
            'updated_at'    => now(),
        ]);
    }
}
