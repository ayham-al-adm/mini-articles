<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $adminRole = Role::findByName('Admin');
        $userRole = Role::findByName('User');

        $adminRole->givePermissionTo('articles.approve.*');
        $userRole->givePermissionTo('articles.approve.own');

        $admin = User::create([
            'name'          => 'Admin',
            'email'         => 'admin@seeder.com',
            'password'      => Hash::make('12345678'),
            'created_at'    => now(),
            'updated_at'    => now(),
        ]);

        $user = User::create([
            'name'          => 'User',
            'email'         => 'user@seeder.com',
            'password'      => Hash::make('12345678'),
            'created_at'    => now(),
            'updated_at'    => now(),
        ]);

        $admin->assignRole($adminRole);
        $user->assignRole($userRole);
    }
}
