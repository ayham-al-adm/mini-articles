<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Article>
 */
class ArticleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'title'             => fake()->text(30),
            'description'       => fake()->text(200),
            'user_id'           => fake()->randomElement(User::pluck('id')),
            'is_approved'       => fake()->boolean(),
            'approved_by'       => fake()->randomElement(User::role('Admin')->pluck('id')),
            'created_at'        => now(),
            'updated_at'        => now()
        ];
    }
}
