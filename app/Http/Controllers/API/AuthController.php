<?php

declare(strict_types=1);

namespace App\Http\Controllers\API;

use App\Http\Requests\ApiLoginRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\Http\Controllers\AccessTokenController;
use Psr\Http\Message\ServerRequestInterface;

class AuthController extends BaseApiController
{
    public function login(ApiLoginRequest $request)
    {
        $credentials = $request->validated();

        if (!Auth::attempt($credentials)) {
            return $this->returnFailResponse([], 401, 'messages.invalid_credentials');
        }

        $user = Auth::user();

        $authObject = $user->createToken('authToken');

        return $this->returnSuccessResponse(['user' => $user->toArray(), 'auth' => $authObject]);
    }
}
