<?php

declare(strict_types=1);

namespace App\Http\Controllers\API;

use App\Services\CommentService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CommentController extends BaseApiController
{
    protected CommentService $commentService;

    public function __construct(CommentService $commentService)
    {
        $this->commentService = $commentService;
    }

    /**
     * index
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $data = $this->commentService->getCommentsWithPagination($request->has('article_id') ? $request->integer('article_id') : null);

        // TODO: Use api resources for the response
        return $this->returnSuccessJsonResponse($data->toArray());
    }
}
