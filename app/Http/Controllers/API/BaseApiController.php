<?php

declare(strict_types=1);

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;

abstract class BaseApiController extends Controller
{
    /**
     * returnSuccessResponse
     *
     * @param  array|null $data
     * @param  int $statusCode
     * @param  string $messageSlug
     * @return JsonResponse
     */
    public function returnSuccessResponse(array|null $data = null, int $statusCode = 200, string $messageSlug = 'messages.success'): JsonResponse
    {
        return response()->json([
            'message'   => __($messageSlug),
            'data'      => $data,
        ], $statusCode);
    }

    /**
     * returnFailResponse
     *
     * @param  array|null $data
     * @param  int $statusCode
     * @param  string $messageSlug
     * @return JsonResponse
     */
    public function returnFailResponse(array|null $data = null, int $statusCode = 400, string $messageSlug = 'messages.fail'): JsonResponse
    {
        return response()->json([
            'message'   => __($messageSlug),
            'data'      => $data,
        ], $statusCode);
    }
}
