<?php

declare(strict_types=1);

namespace App\Http\Controllers\API;

use App\Services\ArticleService;
use Illuminate\Http\JsonResponse;

class ArticleController extends BaseApiController
{
    protected ArticleService $articleService;

    public function __construct(ArticleService $articleService)
    {
        $this->articleService = $articleService;
    }

    /**
     * index
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $data = $this->articleService->getArticlesWithPagination();

        // TODO: Use api resources for the response
        return $this->returnSuccessJsonResponse($data->toArray());
    }
}
