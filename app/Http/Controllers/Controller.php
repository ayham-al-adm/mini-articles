<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;

    /**
     * redirectSuccessResponse
     *
     * @param  string $messageSlug
     * @return RedirectResponse
     */
    public function redirectSuccessResponse(string $messageSlug = 'messages.success'): RedirectResponse
    {
        return redirect()->back()->with('success', __($messageSlug));
    }

    public function returnSuccessJsonResponse(array $data = null, int $statusCode = 200, string $messageSlug = 'messages.success'): JsonResponse
    {
        return response()->json(
            [
                'status'    => 'success',
                'message'   => __($messageSlug),
                'data'      => $data,
            ],
            $statusCode
        );
    }
}
