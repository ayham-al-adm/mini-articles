<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\CommentStoreRequest;
use App\Http\Requests\CommentUpdateRequest;
use App\Services\CommentService;
use Illuminate\Http\Request;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;


class CommentController extends Controller
{
    protected CommentService $commentService;

    public function __construct(CommentService $commentService)
    {
        $this->commentService = $commentService;
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('comments.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        return view('comments.create', ['formInputs' => $this->commentService->getModelInputFields()]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  CommentStoreRequest $request
     * @return RedirectResponse
     */
    public function store(CommentStoreRequest $request): RedirectResponse
    {
        // dd($request->validated());
        $commentData = $request->validated() + ['user_id' => auth()->id()];

        $this->commentService->storeComment($commentData);

        return $this->redirectSuccessResponse();
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $comment = $this->commentService->findByID($id);
        return view('comments.edit', [
            'comment' => $comment,
            'formInputs' => $this->commentService->getModelInputFields()
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(CommentUpdateRequest $request, string $id)
    {
        $this->commentService->updateByID($id, $request->validated());
        return $this->redirectSuccessResponse();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $id
     * @return void
     */
    public function destroy(string $id)
    {
        $this->commentService->deleteByID($id);

        if (request()->ajax())
            return $this->returnSuccessJsonResponse();

        return $this->redirectSuccessResponse();
    }

    /**
     * ajaxIndex
     *
     * @return JsonResponse
     */
    public function ajaxIndex(): JsonResponse
    {
        $data = $this->commentService->getAjaxQueryForDatatable();

        return $this->commentService->buildAjaxDatatable($data);
    }
}
