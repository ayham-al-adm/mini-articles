<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\ArticleStoreRequest;
use App\Http\Requests\ArticleUpdateRequest;
use App\Models\Article;
use App\Services\ArticleService;
use Illuminate\Http\Request;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;


class ArticleController extends Controller
{
    protected ArticleService $articleService;

    public function __construct(ArticleService $articleService)
    {
        $this->articleService = $articleService;
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('articles.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        return view('articles.create', ['formInputs' => $this->articleService->getModelInputFields()]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  ArticleStoreRequest $request
     * @return RedirectResponse
     */
    public function store(ArticleStoreRequest $request): RedirectResponse
    {
        $articleData = $request->validated() + ['user_id' => auth()->id()];

        $this->articleService->storeArticle($articleData);

        return $this->redirectSuccessResponse();
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $article = $this->articleService->findByID($id);

        return view('articles.show', ['article' => $article]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $article = $this->articleService->findByID($id, false);
        return view('articles.edit', [
            'article' => $article,
            'formInputs' => $this->articleService->getModelInputFields()
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(ArticleUpdateRequest $request, string $id)
    {
        $this->articleService->updateByID($id, $request->validated());
        return $this->redirectSuccessResponse();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $id
     * @return void
     */
    public function destroy(string $id)
    {
        $this->articleService->deleteByID($id);

        if (request()->ajax())
            return $this->returnSuccessJsonResponse();

        return $this->redirectSuccessResponse();
    }

    /**
     * ajaxIndex
     *
     * @return JsonResponse
     */
    public function ajaxIndex(): JsonResponse
    {
        $data = $this->articleService->getAjaxQueryForDatatable();

        return $this->articleService->buildAjaxDatatable($data);
    }

    /**
     * approve
     *
     * @param  mixed $article
     * @return JsonResponse
     */
    public function approve(Article $article): JsonResponse
    {
        $this->articleService->approve($article);

        return $this->returnSuccessJsonResponse();
    }
}
