<?php

namespace App\Http\Middleware;

use App\Services\ArticleService;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CheckIfArticleIsApprovable
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $articleService = app()->make(ArticleService::class);

        if (!$articleService->checkIfApprovable($request->article, auth()->user()))
            abort(403, __('messages.no_allowed_action'));

        return $next($request);
    }
}
