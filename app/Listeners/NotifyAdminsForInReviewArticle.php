<?php

namespace App\Listeners;

use App\Events\CreatingNewArticle;
use App\Models\User;
use App\Notifications\NotifyAdminsForInReviewArticle as NotificationsNotifyAdminsForInReviewArticle;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class NotifyAdminsForInReviewArticle implements ShouldQueue
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(CreatingNewArticle $event): void
    {
        $article = $event->article;

        $admins = User::role('admin')->get();

        foreach ($admins as $admin) {
            $admin->notify(new NotificationsNotifyAdminsForInReviewArticle($article));
        }
    }
}
