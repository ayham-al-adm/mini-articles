<?php

declare(strict_types=1);

namespace App\Listeners;

use App\Events\CreatingNewComment;
use App\Events\EditingArticle;
use App\Services\CacheService;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Events\Dispatcher;

class ClearArticleCache
{
    protected CacheService $cacheService;
    /**
     * Create the event listener.
     */
    public function __construct(CacheService $cacheService)
    {
        $this->cacheService = $cacheService;
    }


    public function handleCreatingComment(CreatingNewComment $event): void
    {
        $articleId = $event->comment->article_id;
        $this->cacheService->removeFromCache("articles:{$articleId}");
    }


    public function handleUpdatingArticle(EditingArticle $event): void
    {
        $articleId = $event->article->id;
        $this->cacheService->removeFromCache("articles:{$articleId}");
    }

    /**
     * Register the listeners for the subscriber.
     */
    public function subscribe(Dispatcher $events): void
    {
        $events->listen(
            CreatingNewComment::class,
            [ClearArticleCache::class, 'handleCreatingComment']
        );

        $events->listen(
            EditingArticle::class,
            [ClearArticleCache::class, 'handleUpdatingArticle']
        );
    }
}
