<?php

declare(strict_types=1);

namespace App\Services;

use App\Models\Article;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Yajra\DataTables\DataTables;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Pagination\LengthAwarePaginator;

class ArticleService extends BaseService
{

    protected static $inputFields = [
        'title' => ['type' => 'text', 'required' => true],
        'description'   => ['type' => 'textarea', 'required' => true],
    ];

    protected CacheService $cacheService;

    public function __construct(CacheService $cacheService)
    {
        $this->cacheService = $cacheService;
    }
    /**
     * storeArticle
     *
     * @param  array $data
     * @return Article
     */
    public function storeArticle(array $data): Article
    {
        return Article::create($data);
    }

    /**
     * getModelInputFields
     *
     * @return array
     */
    public function getModelInputFields(): array
    {
        return static::$inputFields;
    }

    /**
     * getAjaxQueryForDatatable
     *
     * @return Builder
     */
    public function getAjaxQueryForDatatable(): Builder
    {
        return Article::select(['id', 'title', 'is_approved', 'created_at', 'updated_at']);
    }

    /**
     * buildAjaxDatatable
     *
     * @param  Builder $data
     * @return JsonResponse
     */
    public function buildAjaxDatatable(Builder $data): JsonResponse
    {
        return DataTables::of($data)
            ->addColumn('created_at', function ($data) {
                return Carbon::parse($data->created_at)->diffForHumans();
            })
            ->addColumn('updated_at', function ($data) {
                return Carbon::parse($data->updated_at)->diffForHumans();
            })
            // TODO: apply same functionality for edit and delete buttons
            ->addColumn('approvable', function ($data) {
                return $this->checkIfApprovable($data, auth()->user());
            })
            ->make(true);
    }


    /**
     * findByID
     *
     * @param  string $id
     * @param  bool $allowReadFromCache
     * @param  bool $allowCache
     * @param  int $cacheMinutes
     * @return Article
     */
    public function findByID(string $id, bool $allowReadFromCache = true, bool $allowCache = true, int $cacheMinutes = 60): Article
    {
        if ($allowReadFromCache && $item = $this->cacheService->retrieveFromCache("articles:{$id}")) {
            return $item;
        }

        $item = Article::with('comments')->findOrFail($id);

        if ($allowCache) {
            $this->cacheService->addToCache("articles:{$id}", $item, $cacheMinutes);
        }

        return $item;
    }

    /**
     * updateByID
     *
     * @param  mixed $id
     * @param  mixed $dataToUpdate
     * @return int number of affected rows
     */
    public function updateByID(string $id, array $dataToUpdate): bool
    {
        $article = Article::findOrFail($id);
        return $article->update($dataToUpdate);
    }

    /**
     * deleteByID
     *
     * @param  string $id
     * @return int
     */
    public function deleteByID(string $id): int
    {
        return Article::destroy($id);
    }

    /**
     * getArticlesWithPagination
     *
     * @return LengthAwarePaginator
     */
    public function getArticlesWithPagination(): LengthAwarePaginator
    {
        return Article::approved()->paginate();
    }

    /**
     * approve
     *
     * @param  Article $article
     * @return bool
     */
    public function approve(Article $article): bool
    {
        $article->is_approved = 1;
        $article->approved_by = auth()->id();

        return $article->save();
    }

    /**
     * checkIfApprovable
     *
     * @param  mixed $data
     * @param  User $user
     * @return bool
     */
    public function checkIfApprovable($data, User $user): bool
    {
        $currentUserIsTheOwnerOrHasPermission = ($data->user_id == $user->id || $user->can('articles.approve.*'));

        return !$data->is_approved && $currentUserIsTheOwnerOrHasPermission;
    }
}
