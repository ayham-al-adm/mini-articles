<?php

declare(strict_types=1);

namespace App\Services;

use App\Models\Article;
use App\Models\Comment;
use Illuminate\Database\Eloquent\Builder;
use Yajra\DataTables\DataTables;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Pagination\LengthAwarePaginator;

class CommentService extends BaseService
{

    protected static $inputFields = [
        'comment' => ['type' => 'textarea', 'required' => true],
        'article_id'   => [
            'type'          => 'foreign',
            'required'      => true,
            'related_model' => Article::class,
        ],
    ];
    /**
     * storeComment
     *
     * @param  array $data
     * @return Comment
     */
    public function storeComment(array $data): Comment
    {
        return Comment::create($data);
    }

    /**
     * getModelInputFields
     *
     * @return array
     */
    public function getModelInputFields(): array
    {
        return static::$inputFields;
    }

    /**
     * getAjaxQueryForDatatable
     *
     * @return Builder
     */
    public function getAjaxQueryForDatatable(): Builder
    {
        return Comment::select(['id', 'comment', 'user_id', 'article_id', 'created_at', 'updated_at']);
    }

    /**
     * buildAjaxDatatable
     *
     * @param  Builder $data
     * @return JsonResponse
     */
    public function buildAjaxDatatable(Builder $data): JsonResponse
    {
        return DataTables::of($data)
            ->addColumn('created_at', function ($data) {
                return Carbon::parse($data->created_at)->diffForHumans();
            })
            ->addColumn('updated_at', function ($data) {
                return Carbon::parse($data->updated_at)->diffForHumans();
            })
            ->make(true);
    }

    /**
     * findByID
     *
     * @param  string $id
     * @return Comment
     */
    public function findByID(string $id): Comment
    {
        return Comment::find($id);
    }

    /**
     * updateByID
     *
     * @param  mixed $id
     * @param  mixed $dataToUpdate
     * @return int number of affected rows
     */
    public function updateByID(string $id, array $dataToUpdate): int
    {
        return Comment::where('id', $id)->update($dataToUpdate);
    }

    /**
     * deleteByID
     *
     * @param  string $id
     * @return int
     */
    public function deleteByID(string $id): int
    {
        return Comment::destroy($id);
    }

    /**
     * getArticlesWithPagination
     *
     * @return LengthAwarePaginator
     */
    public function getCommentsWithPagination(int $articleID = null): LengthAwarePaginator
    {
        $query = Comment::query();

        if ($articleID)
            $query->where('article_id', $articleID);

        return $query->paginate();
    }
}
