<?php

declare(strict_types=1);

namespace App\Services;

use Illuminate\Support\Facades\Cache;

class CacheService extends BaseService
{
    /**
     * addToCache
     *
     * @param  string $cacheKey
     * @param  mixed $value
     * @param  int $expirationMinutes
     * @return bool
     */
    public function addToCache(string $cacheKey, $value, int $expirationMinutes = 60): bool
    {
        return Cache::put($cacheKey, $value, $expirationMinutes);
    }

    /**
     * retrieveFromCache
     *
     * @param  string $cacheKey
     * @return void
     */
    public function retrieveFromCache(string $cacheKey)
    {
        return Cache::get($cacheKey);
    }

    /**
     * removeFromCache
     *
     * @param  string $cacheKey
     * @return bool
     */
    public function removeFromCache(string $cacheKey): bool
    {
        return Cache::forget($cacheKey);
    }
}
