
<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\ArticleController;
use App\Http\Controllers\API\CommentController;
use App\Http\Controllers\API\AuthController;

Route::post('/login', [AuthController::class, 'login'])->name('login.api');

Route::get('/articles', [ArticleController::class, 'index'])->middleware('auth:api');
Route::get('/comments', [CommentController::class, 'index'])->middleware('auth:api');
