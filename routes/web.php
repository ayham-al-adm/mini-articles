<?php

use App\Http\Controllers\ArticleController;
use App\Http\Controllers\CommentController;
use App\Http\Middleware\CheckIfArticleIsApprovable;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/articles/ajax', [ArticleController::class, 'ajaxIndex']);
Route::patch('/articles/{article}/approve', [ArticleController::class, 'approve'])->middleware(CheckIfArticleIsApprovable::class);
Route::resource('articles', ArticleController::class);

Route::get('/comments/ajax', [CommentController::class, 'ajaxIndex']);
Route::resource('comments', CommentController::class);

