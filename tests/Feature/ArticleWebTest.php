<?php

namespace Tests\Feature;

use App\Models\Article;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class ArticleWebTest extends TestCase
{
    /**
     * test__content_type_for_index.
     */
    public function test__content_type_for_index(): void
    {
        $response = $this->get('/');

        $response->assertStatus(200);

        $response->assertHeader('Content-Type', 'text/html; charset=UTF-8');
    }

    /**
     * test__content_type_for_show.
     */
    public function test__content_type_for_show(): void
    {
        $article = Article::first();
        if (!$article) return;

        $response = $this->get('/articles/' . $article->id);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'text/html; charset=UTF-8');
    }

    /**
     * test__content_not_found_show.
     */
    public function test__content_not_found_show(): void
    {
        $response = $this->get('/articles/' . 0);

        $response->assertStatus(404);
        $response->assertHeader('Content-Type', 'text/html; charset=UTF-8');
    }
}
