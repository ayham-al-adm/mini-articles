<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\TestCase;

class ArticleAPIsTest extends TestCase
{
    // TODO: apply mocks for tests to keep data clear
    /**
     * test_unauthorized_index.
     */
    public function test_unauthorized_api_index(): void
    {
        $response = $this->get('/api/v1/articles', [
            'Content-Type'  => 'application/json',
            'Accept'        => 'application/json'
        ]);

        $response->assertStatus(401);

        $response->assertUnauthorized();
    }

    /**
     * test_unauthorized_index.
     */
    public function test_authorized_api_index(): void
    {
        $user = User::first();

        $response = $this->actingAs($user, 'api')->get('/api/v1/articles', [
            'Content-Type'  => 'application/json',
            'Accept'        => 'application/json'
        ]);

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'status',
            'message',
            'data' => [
                'current_page',
                'data',
                'first_page_url',
                'from',
                'last_page',
                'last_page_url',
                'links' => [
                    '*' => [
                        'url',
                        'label',
                        'active',
                    ],
                ],
                'next_page_url',
                'path',
                'per_page',
                'prev_page_url',
                'to',
                'total',
            ],
        ]);
    }
}
