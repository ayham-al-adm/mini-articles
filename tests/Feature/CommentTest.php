<?php

namespace Tests\Feature;

use App\Models\Article;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CommentTest extends TestCase
{
    /**
     * A basic feature test example.
     */
    public function test_success_comment(): void
    {

        $article = Article::first();
        $user = User::first();

        if (!$article || !$user) return;

        $response = $this->actingAs($user)->post('/comments', [
            'article_id'        => $article->id,
            'comment'           => 'test comment'
        ]);

        // Redirect back
        $response->assertStatus(302);
        $response->assertHeader('Content-Type', 'text/html; charset=UTF-8');
    }
}
