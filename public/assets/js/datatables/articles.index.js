const tableId = "index_table";
function deleteRow(id) {
    // TODO: replace this confirmation by bootstrap modal
    var confirmation = confirm("Are you sure you want to delete this row?");

    if (confirmation) {
        var csrfToken = $("meta[name='csrf-token']").attr("content");

        $.ajax({
            url: "/articles/" + id,
            type: "DELETE",
            headers: {
                "X-CSRF-TOKEN": csrfToken,
            },
            success: function (response) {
                $(`#${tableId}`).DataTable().ajax.reload();
            },
            error: function (xhr, status, error) {
                // TODO: Handle error response
            },
        });
    }
}

function approveArticle(articleID) {
    var csrfToken = $("meta[name='csrf-token']").attr("content");

    $.ajax({
        url: `/articles/${articleID}/approve`,
        type: "PATCH",
        headers: {
            "X-CSRF-TOKEN": csrfToken,
        },
        success: function (response) {
            $(`#${tableId}`).DataTable().ajax.reload();
        },
        error: function (xhr, status, error) {
            // TODO: Handle error response
        },
    });
}

$(`#${tableId}`).DataTable({
    ajax: "/articles/ajax",
    processing: true,
    serverSide: true,
    columns: [
        {
            data: "id",
        },
        {
            data: "title",
        },
        {
            data: "is_approved",
        },
        {
            data: "created_at",
        },
        {
            data: "updated_at",
        },
        {
            data: null,
            render: function (data, type, row) {
                return (
                    `<a href="/articles/${data.id}" class="btn btn-success btn-sm mx-1">Show</a>` +
                    `<a href="/articles/${data.id}/edit" class="btn btn-primary btn-sm mx-1">Edit</a>` +
                    '<button class="btn btn-danger btn-sm mx-1" onclick="deleteRow(' +
                    data.id +
                    ')">Delete</button>' +
                    (data.approvable
                        ? `<button class="btn btn-primary btn-sm mx-1" onclick="approveArticle('${data.id}')">Approve</button>`
                        : "")
                );
            },
        },
    ],
});
