const tableId = "index_table";
function deleteRow(id) {
    // TODO: replace this confirmation by bootstrap modal
    var confirmation = confirm("Are you sure you want to delete this row?");

    if (confirmation) {
        var csrfToken = $("meta[name='csrf-token']").attr("content");

        $.ajax({
            url: "/comments/" + id,
            type: "DELETE",
            headers: {
                "X-CSRF-TOKEN": csrfToken,
            },
            success: function (response) {
                $(`#${tableId}`).DataTable().ajax.reload();
            },
            error: function (xhr, status, error) {
                // TODO: Handle error response
            },
        });
    }
}

$(`#${tableId}`).DataTable({
    ajax: "/comments/ajax",
    processing: true,
    serverSide: true,
    columns: [
        {
            data: "id",
        },
        {
            data: "comment",
        },
        {
            data: "user_id",
        },
        {
            data: "article_id",
        },
        {
            data: "created_at",
        },
        {
            data: "updated_at",
        },
        {
            data: null,
            render: function (data, type, row) {
                return (
                    `<a href="/comments/${data.id}" class="btn btn-success btn-sm mx-1">Show</a>` +
                    `<a href="/comments/${data.id}/edit" class="btn btn-primary btn-sm mx-1">Edit</a>` +
                    '<button class="btn btn-danger btn-sm mx-1" onclick="deleteRow(' +
                    data.id +
                    ')">Delete</button>'
                );
            },
        },
    ],
});
